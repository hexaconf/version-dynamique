<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le mot de passe doit être d\'une longueur de 6 caractères et correspondre à la confirmation.',
    'reset' => 'Votre mot de passe a été réinitialisé avec succès !',
    'sent' => 'Nous vous avons envoyé un e-mail de confirmation, veuillez consulter votre messagerie !',
    'token' => 'La clé de réinitialisation de votre mot de passe est invalide.',
    'user' => "Désolé, cette adresse e-mail nous est inconnue.",

];
