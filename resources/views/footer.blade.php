<div class="row">
    <div class="sitemap hide-for-small medium-4 large-4 columns">
        <a href="{{ url('/mentions-legales') }}">Mentions légales</a>
        <a href="{{ url('/contact') }}">Contact</a>
        <a href="">Plan du site</a>
    </div>
    <div class="small-12 medium-5 columns center no-padding">
        <img src="{{ asset('img/logo-hexaconf-medium.png') }}" alt="Hexaconf"> avec
        <img src="{{ asset('img/logo_couleur_sans_fond.png') }}" alt="Logo IUT Informatique Bordeaux" width="100" height="100">
        <p class="show-for-small-only">Retrouvez nous sur twitter ! <a href="index.html"><img src="{{ asset('img/ico_twitter.png') }}"
                                                                                              alt="Icone twitter"/></a></p>
    </div>
    <div class="small-12 show-for-small-only columns links center">
        <a href="{{ url('/mentions-legales') }}">Mentions légales</a>
        <a href="{{ url('/contact') }}">Contact</a>
        <a href="">Plan du site</a>
    </div>
    <div class="hide-for-small medium-3 columns text-right">
        <div class="row social-link">
            <div class="medium-12 large-12 columns">
                <a href="https://twitter.com/HexaConf"> @HexaConf <img src="{{ asset('img/ico_twitter.png') }}"
                                                                       alt="Icone twitter"/></a>
            </div>
            <div class="medium-12 large-12 columns">
                <a href="mailto:hexaconf@gmail.com"> hexaconf@gmail.com <img src="{{ asset('img/ico_email.png') }}"
                                                                             alt="Icone mail"/> </a>
            </div>
        </div>
    </div>
</div>