        @extends('master')

@section('title')
    Agenda
@endsection

@section('content')

    <main>
        <h1>Agenda</h1>

        <div id="agenda">
            <div class="row" id="years">
                <p>2010</p>
                <p>2011</p>
                <p>2012</p>
                <p>2013</p>
                <p>2014</p>
                <p>2015</p>
                <p>2016</p>
                <p>2017</p>
            </div>
            <div class="row" id="months">
                <p>Janvier</p>
                <p>Fevrier</p>
                <p>Mars</p>
                <p>Avril</p>
                <p>Mai</p>
                <p>Juin</p>
                <p>Juillet</p>
                <p>Août</p>
                <p>Septembre</p>
                <p>Octobre</p>
                <p>Novembre</p>
                <p>Décembre</p>
            </div>
            <div class="row" id="days">
                <p>1</p><p>2</p><p>3</p><p>4</p><p>5</p>
                <p>6</p><p>7</p><p>8</p><p>9</p><p>10</p>
                <p>11</p><p>12</p><p>13</p><p>14</p><p>15</p>
                <p>16</p><p>17</p><p>18</p><p>19</p><p>20</p>
                <p>21</p><p>22</p><p>23</p><p>24</p><p>25</p>
                <p>26</p><p>27</p><p>28</p><p>29</p><p>30</p>
                <p>31</p><span id="remove" class="icon-remove"></span>
            </div>
        </div>

        <div class="row catalogue">
            <div class="grid">
                @foreach($events as $event)
                    <div class="{{ $event->getDateAsCSSClass() }}">
                        <div class="small-12 medium-6 large-4 columns">
                            <div class="panel conf-big-panel" data-href="{{url('evenements/'.$event->id)}}">
                                @include('event/medium', array('event' => $event))
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <h2>Désolé, il n'y a plus d'évènement correspondant à votre recherche</h2>
        </div>


    </main>

@endsection


@section('scripts')
    <script src="{{ asset('js/agenda.js') }}"></script>
@endsection
