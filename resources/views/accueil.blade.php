@extends('master')

@section('title')
    Accueil
@endsection

@section('content')

    <main class="accueil">
        <div id="accueil">
            <div class="row">
                <div class="small-12 medium-8 medium-centered columns">
                    <div class="panel">
                        <h1>Hexaconf, le portail des conférences du Web</h1>
                        Vous êtes développeur, futur développeur ou simple geek curieux à propos des nouvelles technologies
                        ?
                        Alors vous êtes à la bonne adresse ! Sur ce portail, vous pourrez :
                        <ul>
                            <li>Découvrir les prochains événements de conférences sur le Web</li>
                            <li>Noter les événements</li>
                            <li>Recevoir des alertes sur les prochains événements à venir</li>
                        </ul>
                        <div class="text-center">
                            <a href="{{ "evenements" }}" class="button">Voir les conférences</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" data-equalizer>
            <div class="medium-8 large-6 columns" data-href="{{ "evenements/" . $bestEvent->id }}">
                <div class="panel conf-big-panel" data-equalizer-watch>
                    <h2>Événement le mieux noté</h2>
                    @include('event/large', ['event' => $bestEvent])
                </div>
            </div>
            <div class="medium-8 large-6 columns" data-href="{{ 'evenements/' . $nextEvent->id  }}">
                <div class="panel conf-big-panel" data-equalizer-watch>
                    @if($nextEvent !== null)
                        <h2>Prochain événement</h2>
                        @include('event/large', ['event' => $nextEvent])
                    @else
                        <b>Aucune prochaine conférence n'est prévue pour le moment !</b>
                    @endif

                </div>
            </div>
        </div>
    </main>
@endsection
