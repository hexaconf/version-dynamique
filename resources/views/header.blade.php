<nav>
    <div class="row">
        <!-- mobile header -->
        <div class="small-3 columns hide-for-large-up">
            <section id="menu" class="left-small">
                <div class="menu-icon">
                    <span>
                    </span>
                </div>
                <input id="button-open-input" type="radio" name="menuButton">
                <input id="button-close-input" type="radio" name="menuButton" checked="">
                <ul class="mobile-nav">
                    <li class="mobile-nav-item"><a href="{{ url('/') }}">Accueil</a></li>
                    <li class="mobile-nav-item"><a href="{{ url('/evenements') }}">Événements</a></li>
                    <li class="mobile-nav-item"><a href="{{ url('/evenements/agenda') }}">Agenda</a></li>
                    <li class="mobile-nav-item"><a href="{{ url('/evenements/ajouter') }}">Proposer un événement</a>
                    </li>
                    <li class="mobile-nav-item">
                        <form class="row" method="post" action="{{ url('/evenements/rechercher') }}">
                            {{ csrf_field() }}
                            <div class="small-3 columns no-padding">
                                <a href="#" class="search-button">
                                    <img src="{{ asset('img/ico_search.svg') }}" alt="search icon" width="30px"
                                         height="30px">
                                </a>
                            </div>
                            <div class="small-9 columns no-padding-left">
                                <input type="text" name="query">
                            </div>
                        </form>
                    </li>
                    <li class="mobile-nav-item account">

                        <a href="{{ url('account') }}">
                            @if(Auth::check())
                                {{ Auth::user()->name() }}
                            @else
                                Mon Compte
                            @endif
                            <img src="{{ asset('img/ico_perso.png') }}" alt="personal account">
                        </a><br/>
                        @if(Auth::check())
                            <a href="{{ url('/auth/deconnexion') }}">Se déconnecter</a>
                        @else
                            <a data-reveal-id="connect-modal">Se connecter
                                <img src="{{ asset('img/ico_perso.png') }}" alt="personal account">
                            </a>
                        @endif
                    </li>
                </ul>
            </section>
        </div>
        <!-- desktop header -->
        <div class="small-8 small-centered large-2 large-uncentered  columns ">
            <a href="{{ url('/') }}">
                <img src="{{ asset('img/logo-hexaconf-small.png') }}" alt="Logo d'Hexaconf" width="160" height="36">
            </a>
        </div>
        <div class="large-7 columns hide-for-medium-down">
            <ul>
                <li><a href="{{ url('/') }}">Accueil</a></li>
                <li><a href="{{ url('/evenements') }}">Événements</a></li>
                <li><a href="{{ url('/evenements/agenda') }}">Agenda</a></li>
                <li><a href="{{ url('/evenements/ajouter') }}">Proposer un événement</a></li>
                <li>
                    <form method="post" action="{{ url('/evenements/rechercher') }}">
                        {{ csrf_field() }}
                        <input type="text" class="search-input" name="query">
                    </form>
                </li>
            </ul>
        </div>
        <div class="large-3 columns hide-for-medium-down">
            <a href="{{ url('account') }}">
                <img src="{{ asset('img/ico_perso.png') }}" alt="personal account" width="35px" height="35px"
                     id="account">
                @if(Auth::check())
                    {{ Auth::user()->name() }}
                @else
                    Mon Compte
                @endif
            </a>

            <div id="account-popup">
                @if(Auth::check())
                    <a href="{{ url('/account') }}">Paramètres du compte</a>
                    @if(Auth::user()->status == 1)
                        <a href="{{ url('/admin/') }}">Administration</a>
                    @endif
                    <a href="{{ url('/auth/deconnexion') }}">Se déconnecter</a>
                @else
                    <a data-reveal-id="connect-modal">Se connecter</a>
                    <a data-reveal-id="register-modal">S'inscrire</a>
                @endif
            </div>
        </div>

    </div>
</nav>