<div class="row" id="{{ $event->html_id }}">
    <div class="large-12 columns text-center">
        <h3>{{ $event->title }}</h3>
    </div>

    <div class="large-12 columns text-center">
        <div class="row">
            @if(!empty($event->presentation_picture))
            <div class="large-8 columns text-center">
                <img src="{{ asset('img/pictures/normal/'.$event->presentation_picture) }}" alt="presentation_picture">
            </div>
            @endif
            @if($event->average_note != -1)
            <div class="large-4 columns text-center">
                <div class="note-{{round($event->average_note)}} small inline-block" title="Note de la conférence">
                    <sup>{{ $event->average_note }}</sup>&frasl;<sub>6</sub>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="large-12 columns">
        <div class="row">
            <div class="small-12 large-12 columns">
                <p>
                    <img src="{{ asset('img/ico_calendrier.png') }}" alt="Icone calendrier">
                    {{ $event->getDatesForHumans() }}
                </p>
                <p>
                    <img src="{{ asset('img/ico_localisation.png') }}" alt="Icone localisation"> {{ $event->address }}
                </p>
            </div>
        </div>
    </div>
</div>
<div class="text-center">
    <a href="{{ 'evenements/'.$event->id }}" class="button button-small">Plus de détails</a>
</div>