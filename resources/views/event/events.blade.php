@extends('master')

@section('title')
    Tous les événements
@endsection


@section('content')

    <main>
        <h1>Tous les événements</h1>
        <div id="search" class="row">
            <aside class="medium-12 columns">
                <form class="panel" method="get" action="{{ url('evenements/recherche_avancee') }}">
                    {{ csrf_field() }}
                    <h3>Affiner la recherche</h3>
                    <div class="row">
                        <div class="large-3 columns text-center">
                            <label for="title">Titre :</label>
                            <input type="text" name="title" id="title" value="{{ isset($input)? $input['title']:''  }}">
                        </div>
                        <div class="large-3 columns text-center">
                            <label for="date">Prochaine date programmée :</label>
                            <input type="date" name="date" id="date" value="{{ isset($input)? $input['date']:''  }}">
                        </div>
                        <div class="large-3 columns text-center">
                            <label for="address">Adresse :</label>
                            <input type="text" name="address" id="address" value="{{ isset($input)? $input['address']:''  }}">
                        </div>
                        <div class="large-3 columns text-center">
                            <input type="submit" class="button" value="Affiner la recherche">
                        </div>
                    </div>
                </form>
            </aside>
        </div>

        <div class="row catalogue">
            <section class="medium-12 columns">
                <div class="row">
                    @foreach($events as $event)
                        <div class="small-12 medium-6 large-4 end columns">
                            <div class="panel conf-medium-panel" data-href="{{url('evenements/'.$event->id)}}">
                                @include('event/medium', array('event' => $event))
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
        </div>
    </main>
    <a id="map-anchor" href="#" data-reveal-id="mapModal" title="Événements sur la carte">
        <img src="{{ asset('img/ico_map_france.png') }}" draggable="false" alt="Icone carte de France"/>
    </a>
    <div id="mapModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <h2 id="modalTitle">Tous les événements sur la carte</h2>
        <a class="close-reveal-modal" aria-label="Fermer">&#215;</a>
        <div id="map"></div>
    </div>
@endsection

@section('scripts')
    <script type="application/javascript">
        var urlCoords = '{{ url('gpscoords') }}';
    </script>
    <script type="application/javascript" src="{{ asset('js/google-maps-api.js') }}"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env('MAPS_SERVER_KEY', 'AIzaSyDl9nbwgZyE5J53whoOIItZBFNHBMv91Zc') }}&callback=initMap"></script>
@endsection