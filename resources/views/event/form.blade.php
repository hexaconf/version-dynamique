@extends('master')

@section('title')
    @if($isUpdate)
        Modifier l'événement {{ $event->title }}
    @else
        Proposer un événement
    @endif

@endsection

@section('content')

    <main id="form">
        @if($isUpdate)
            <h1>Modifier l'événement</h1>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if($isUpdate)
            {!! Form::open(array('route' => array('events.update', $event->id), 'enctype' => 'multipart/form-data')) !!}
        @else
            {!! Form::open(array('route' => 'events.store', 'enctype' => 'multipart/form-data')) !!}
        @endif

        <div class="row">

            <div class="columns">
                <h2>Informations principales</h2>

                <div class="row">
                    <div class="medium-4 large-3 columns"><label for="title">Titre de la conférence</label></div>
                    <div class="medium-6 large-5 columns">
                        {!! Form::text('title', old('title', $event->title), array('placeholder' => 'Titre de la conférence', 'required' => 'required')) !!}
                    </div>
                    <div class="medium-12 large-4 columns error" id="title-error"></div>
                </div>

                <div class="row">
                    <div class="medium-4 large-3 columns"><label for="subject">Sujet</label></div>
                    <div class="medium-6 large-5 columns">
                        {!! Form::text('subject', old('subject', $event->subject), array('placeholder' => 'Sujet')) !!}
                    </div>
                    <div class="medium-12 large-4 columns error" id="subject-error"></div>
                </div>

                <div class="row">
                    <div class="small-3 large-1 columns">
                        <img src="{{ asset('img/ico_iut.png') }}" alt="Icone localisation">
                    </div>
                    <div class="small-9 large-7 columns">
                        {!! Form::text('address', old('address', $event->address), array('placeholder' => 'Adresse de l\'évènement', 'required' => 'required')) !!}
                    </div>
                    <div class="medium-12 large-4 columns error" id="address-error"></div>

                </div>

                <div class="row">
                    <div class="small-3 large-1 columns"><img src="{{ asset('img/ico_calendrier.png') }}"
                                                              alt="Icone calendrier"></div>
                    <div class="small-9 large-3 columns left">
                        {!! Form::text('begin_date', old('begin_date', $event->begin_date), array('id' => 'begin_date', 'placeholder' => 'Date de début', 'required' => 'required')) !!}
                    </div>
                    <div class="small-3 large-1 columns middle">au</div>
                    <div class="small-9 large-3 columns left">
                        {!! Form::text('end_date', old('end_date', $event->end_date), array('id' => 'end_date', 'placeholder' => 'Date de fin')) !!}
                    </div>

                    <div class="medium-12 large-4 columns error" id="date-error"></div>

                </div>

                <div class="row">
                    <div class="small-3 large-2 columns"><img src="{{ asset('img/ico_horloge.png') }}"
                                                              alt="Icone horloge">
                    </div>
                    <div class="small-3 large-2 columns">
                        {!! Form::time('begin_hour', old('begin_hour', $event->begin_hour), array('min' => 0, 'max'=>24, 'placeholder' => '00')) !!}
                    </div>
                    <div class="small-3 large-2 columns middle">jusqu'à</div>
                    <div class="small-3 large-2 columns end">
                        {!! Form::time('end_hour', old('end_hour', $event->end_hour), array('min' => 0, 'max'=>24, 'placeholder' => '00')) !!}
                    </div>
                    <div class="medium-12 large-4 columns error" id="hours-error"></div>
                </div>

                <div class="row">
                    <div class="small-8 large-4 columns right validate">
                        <div class="button next">Prochaine étape</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="columns">
                <h2>Prix et mots clés</h2>

                <div class="row">
                    <div class="small-12 large-12 columns">
                        <label for="price_value">Les prix</label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 small-12 price-fields columns">
                        <div class="fields">
                            @if(Input::old('price_value[]') != 0)

                                @for($i = 0; $i< count(old('price_value[]')); $i++ )
                                    <div class="row">
                                        <div class="small-1 columns">
                                            <a href="#" class="delete-price">x</a>
                                        </div>
                                        <div class="small-5 large-4 columns">
                                            {!! Form::number('price_value[]', Input::old('price_value[]')->get($i), array('min' => '0', 'placeholder' => 'prix')) !!}
                                        </div>
                                        <div class="small-5 large-5 columns">
                                            {!! Form::text('price_label[]', Input::old('price_label[]')->get($i) ,array('placeholder' => 'Categorie')) !!}
                                        </div>
                                    </div>
                                @endfor
                            @elseif( $event->prices->count() > 0 )
                                @for($i = 0; $i< count($event->prices); $i++ )
                                    <div class="row">
                                        <div class="small-1 columns">
                                            <a href="#" class="delete-price">x</a>
                                        </div>
                                        <div class="small-6 large-4 columns">
                                            {!! Form::number('price_value[]', $event->prices[$i]->value, array('min' => '0', 'placeholder' => 'Prix')) !!}
                                        </div>
                                        <div class="small-6 large-5 columns">
                                            {!! Form::text('price_label[]', $event->prices[$i]->label, array('placeholder' => 'Categorie')) !!}
                                        </div>
                                    </div>
                                @endfor
                            @endif
                        </div>
                        <div class="row">
                            <div class="small-12 large-12 columns large-centered">
                                <div class="button" id="add-price-field">Ajouter un prix</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="large-2 columns"><label for="tags">Tags</label></div>
                    <div class="large-10 columns end">
                        {!! Form::text('tags', '', array('id' => 'mySingleField')) !!}
                        <ul class="tagit ui-widget ui-widget-content ui-corner-all" id="singleFieldTags">
                            @foreach($event->tags as $tag)
                                <li data-value="{{ $tag->title }}">{{ $tag->title }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 large-8 columns right validate">
                        <div class="button previous">Etape précédente</div>
                        <div class="button next">Prochaine étape</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="columns">
                <h2>Informations complémentaires</h2>

                <div class="row">
                    <div class="small-3 large-2 columns">
                        <img src="{{ asset('img/ico_perso.png') }}" alt="Icone groupe de personnes">
                    </div>
                    <div class="radio-boutons">
                        {!! Form::radio('seating_capacity', '10', array('id' => "10")) !!}
                        <label for="10">10-19</label>
                        {!! Form::radio('seating_capacity', '20', array('id' => "20")) !!}
                        <label for="20">20-49</label>
                        {!! Form::radio('seating_capacity', '50', array('id' => "50")) !!}
                        <label for="50">50-99</label>
                        {!! Form::radio('seating_capacity', '100+', array('id' => "100")) !!}
                        <label for="100">100+</label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-5 columns">
                        <label for="edition">Nombre d'éditions précédentes</label>
                    </div>
                    <div class="large-2 columns end">
                        @if(isset($event->edition))
                            {!! Form::number('edition', $event->edition or old('edition') or 0, array('min' => '0')) !!}
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="small-5 large-5 columns">
                        <img src="{{ asset('img/ico_langue.png') }}" alt="Icone drapeau">Langue
                    </div>
                    <div class="small-7 large-7 columns">
                        {!! Form::select('language_id', \App\Language::lists('title', 'id'),  old('language_id', $event->language_id))  !!}
                    </div>
                </div>
                <div class="row">
                    <div><label for="presentation_picture_id" class="button">Image de présentation</label>
                        <input type="file" name="presentation_picture" id="presentation_picture_id">
                    </div>
                    <div>Ou mieux, une vidéo de présentation ? lien youtube par ex.
                        {!! Form::text('presentation_video', old('presentation_video', $event->presentation_video)) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 large-8 columns right validate">
                        <div class="button previous">Etape précédente</div>
                        <div class="button next">Prochaine étape</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="columns">
                <h2>Liens et description de l'évènement</h2>

                <div class="row">
                    <div class="small-3 large-2 columns">
                        <img src="{{ asset('img/ico_site.png') }}" alt="Icone monde">
                    </div>
                    <div class="small-9 large-10 columns">
                        {!! Form::text('website', old('website', $event->website), array('placeholder' => 'Site de l\'évènement')) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 columns">Contenu en ligne</div>
                    <div class="large-9 columns">
                        {!! Form::text('content_link', old('content_link', $event->content_link), array('placeholder' => 'Contenu en ligne')) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="small-3 large-2 columns">
                        <img src="{{ asset('img/ico_email.png') }}" alt="Icone email">
                    </div>
                    <div class="small-9 large-10 columns">
                        {!! Form::email('mail', old('mail', $event->mail), array('placeholder' => 'Adresse email')) !!}
                    </div>
                </div>
                <div class="row">
                    <label>Liens vers les réseaux sociaux</label>
                    {!! Form::text('facebook_link',  old('facebook_link', $event->facebook_link), array('placeholder' => 'Facebook')) !!}
                    {!! Form::text('twitter_link',  old('twitter_link', $event->twitter_link), array('placeholder' => 'Twitter')) !!}
                    {!! Form::text('youtube_link',  old('youtube_link', $event->youtube_link), array('placeholder' => 'Youtube')) !!}
                </div>
                <div class="row">
                    <label>Descriptif</label>
                    {!! Form::textarea('description', old('description', $event->description), array('placeholder' => 'Descriptif de l\évènement', 'cols' => '30', 'rows' => '10')) !!}
                </div>

                <div class="small-12 small-centered large-8 large-centered columns right validate">
                    <div class="button previous">Etape précédente</div>
                    <button type="submit">
                        Envoyer
                    </button>
                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </main>

    <div class="row" style="display:none;" id="newPrice">
        <div class="small-1 columns">
            <a href="#" class="delete-price">x</a>
        </div>
        <div class="small-6 large-4 columns">
            {!! Form::number('price_value[]', '', array('min' => '0', 'placeholder' => 'Prix')) !!}
        </div>
        <div class="small-6 large-5 columns">
            {!! Form::text('price_label[]', '', array('placeholder' => 'Categorie')) !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $("#singleFieldTags").tagit();
        });
    </script>

    <script src="{{ asset('js/validation.js') }}"></script>
@endsection
