<div class="row">
    @if(!empty($event->presentation_picture))
    <div class="large-12 columns">
        <img src="{{ asset('img/pictures/normal/'.$event->presentation_picture) }}" alt="presentation_picture">
    </div>
    <hr />
    @endif
    <div class="large-6 columns">
        <h3>{{ $event->title }}</h3>
        <p>
            {{ $event->short_description }}
        </p>
    </div>
    <div class="large-6 columns no-padding">
        <div class="row">
            @if($event->average_note != -1)
            <div class="small-12 large-12 columns text-center">
                <div class="note-{{round($event->average_note)}} small inline-block" title="Note de l'événement">
                    <sup>{{ $event->average_note }}</sup>&frasl;<sub>6</sub>
                </div>
            </div>
            @endif
            <div class="small-6 large-12 columns no-padding">
                <p>
                    <img src="{{ asset('img/ico_calendrier.png') }}" alt="Icone calendrier">
                    {{ $event->getDatesForHumans() }}
                </p>
            </div>
            <div class="small-6 large-12 columns no-padding">
                <p>
                    <img src="{{ asset('img/ico_localisation.png') }}" alt="Icone localisation">
                    {{ $event->address }}
                </p>
            </div>
        </div>
    </div>
</div>

<div class="text-center">
    <a href="{{ 'evenements/' . $event->id }}" class="button button-small">Plus de détails</a>
</div>