@extends('master')

@section('title')
    {{ $event->title }}
@endsection

@section('content')
    <main>
        <section>
            <div class="row">
                <div class="large-12 columns">
                    <div class="row">
                        <div class="large-8 columns small-9">
                            <h1 class="small-text-left big">{{$event->title}}</h1>
                        </div>
                        <div class="large-4 columns small-3">
                            @if($event->average_note != -1)
                                <div class="note-{{round($event->average_note)}} small inline-block"
                                     title="Note moyenne de l'événement">
                                    @if(\Carbon\Carbon::today() > $event->end_date)
                                        <a href="{{ route('opinion.store') }}" data-reveal-id="addOpinion" class="white-link" title="Noter l'événement">
                                            <sup>{{ $event->average_note }}</sup>&frasl;<sub>6</sub>
                                        </a>
                                    @else
                                        <sup>{{ $event->average_note }}</sup>&frasl;<sub>6</sub>
                                    @endif

                                </div>
                            @else
                                <div class="note-0 small inline-block"
                                     title="Note moyenne de l'événement">
                                    @if(\Carbon\Carbon::today() > $event->end_date)
                                        <a href="{{ route('opinion.store') }}" data-reveal-id="addOpinion" class="white-link" title="Noter l'événement">
                                            ?
                                        </a>
                                    @else
                                        ?
                                    @endif
                                </div>
                            @endif
                        </div>

                    </div>
                </div>

                <div class="large-8 medium-6 column">
                    <h3>{{$event->subject }}</h3>
                </div>
                <div class="large-4 medium-6 columns small-12 small-text-left large-text-right">
                    <h4 class="inline-block">Partagez cet évènement &#8658;</h4>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=hexaconf.fr/evenements/{{ $event->id }}"
                       target="_blank">
                        <img src="{{ asset('img/ico_facebook.png') }}" alt="icône facebook">
                    </a>
                    <a href="https://twitter.com/intent/tweet/?url=hexaconf.fr/evenements/{{ $event->id }}&text='Venez participer à l'évènement {{$event->title}} !'&via=hexaconf"
                       target="_blank">
                        <img src="{{ asset('img/ico_twitter.png') }}" alt="icône twitter">
                    </a>
                </div>

                <hr/>

                <div class="small-12 columns">
                    <div class="row">
                        <div class="medium-5 columns">
                            <img src="{{ asset('img/ico_euro.png') }}" alt="icône prix">
                            @if($event->prices->count() > 1)
                                Tous les prix
                                <ul>
                                    @foreach($event->prices as $price)
                                        <li><b>Catégorie {{ $price->label }}</b> : {{ $price->value }}€</li>
                                    @endforeach
                                </ul>
                            @elseif($event->prices->count() == 1)
                                @if($event->prices->first()->value == 0)
                                @else
                                    {{ $event->prices->first()->value }} €
                                    @if(!empty($event->prices->first()->label))
                                        (catégorie {{ $event->prices->first()->label }})
                                    @endif
                                @endif
                            @else
                                Inconnu
                            @endif
                        </div>
                        <div class="medium-7 columns">
                            <p><img src="{{ asset('img/ico_calendrier.png') }}" alt="icône date">
                                {{ $event->getDatesForHumans() }}
                                @if(! empty($event->begin_hour))
                                    <img src="{{ asset('img/ico_horloge.png') }}" alt="icône heure">
                                    {{$event->begin_hour}}
                                    @if(!empty($event->end_hour))
                                        à {{$event->end_hour}}
                                    @endif
                                @endif
                            </p>
                        </div>
                    </div>
                </div>

                <div class="large-7 columns large-uncentered">
                    <div class="row">
                        <div class="large-12 columns small-12">
                            <p>
                                <img src="{{ asset('img/ico_localisation.png') }}" alt="icône localisation">
                                {{$event->address or 'Adresse non renseignée'}}
                            </p>
                        </div>
                    </div>
                    <a class="button button-small show-for-small" href="http://maps.google.com?q={{$event->address}}">Voir sur Google Map</a>
                    <div class="row">
                        <div class="hide-for-small-only">
                            <div class="large-12 columns small-12">
                                <div class="map">
                                    <iframe src="https://www.google.com/maps/embed/v1/place?key={{ env('MAPS_SERVER_KEY', '') }}&q={{urlencode($event->address)}}"
                                            width="550" height="350" title="map"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (count($event->tags) !=  0)
                        <div class="row">
                            <div class="large-12 columns small-12">
                                <p>Tags :
                                    @foreach ($event->tags as $tag)
                                        <a href="{{ url('/evenements/rechercher/tag/'.$tag->title) }}">#{{ $tag->title }}</a>
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    @endif
                    <div class="show-for-small">
                        <hr/>
                    </div>
                    <div >
                        <div class="row">
                            <div class="large-12 columns small-12">
                                <p><img src="{{ asset('img/ico_perso.png')}}" alt="icône de personne">
                                    {{$event->seating_capacity}} personnes
                                </p>
                            </div>
                        </div>

                        @if ($event->edition != '')
                            <div class="row">
                                <div class="large-12 columns small-12">
                                    <p><img src="{{ asset('img/ico_anne.png') }}" alt="icône de l'année">
                                        @if($event->edition == 0)
                                            Première édition !
                                        @else
                                            {{  ($event->edition+1) }}<sup>ème</sup> édition
                                        @endif
                                    </p>
                                </div>
                            </div>
                        @endif

                        @if(!empty($event->language))
                            <div class="row">
                                <div class="large-12 columns">
                                    <p><img src="{{ asset('img/ico_langue.png') }}" alt="icône langue"> Langue
                                        : {{ ucfirst($event->language->title) }}</p>
                                </div>
                            </div>
                        @endif
                        @if(!empty($event->content_link))
                            <div class="row">
                                <div class="large-12 columns">
                                    <img src="{{ asset('img/ico_site.png') }}" alt="icône internet">
                                    <a href="http://{{$event->content_link}}">Voir le contenu en ligne</a>
                                </div>
                            </div>
                        @endif
                    </div>
                    <hr>
                </div>

                <div class="large-5 columns">
                    <div>
                        @if( isset($event->user) && $event->user->status != 1)
                            <div class="row">
                                <div class="large-12 columns small-12">
                                    <p><img src="{{ asset('img/icon_conferencier.png') }}"
                                            alt="icône conferencier">{{ $event->user->name() }}</p>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            @if(!empty($event->facebook_link) || !empty($event->twitter_link) || !empty($event->youtube_link) || !empty($event->mail))
                            <div class="small-7 medium-4 large-7 columns">
                                <h4>Retrouver l'événement :</h4>
                            </div>
                            @endif
                            @if(!empty($event->facebook_link))
                                <a href="{{$event->facebook_link}}">
                                    <img src="{{ asset('img/ico_facebook.png') }}" alt="icône facebook">
                                </a>
                            @endif
                            @if(!empty($event->twitter_link))
                                <a href="{{$event->twitter_link}}">
                                    <img src="{{ asset('img/ico_twitter.png') }}" alt="icône twitter">
                                </a>
                            @endif
                            @if(!empty($event->youtube_link))
                                <a href="{{$event->youtube_link}}">
                                    <img src="{{ asset('img/ico_youtube.png') }}" alt="icône youtube">
                                </a>
                            @endif
                            @if(!empty($event->mail))
                                <a href="{{ $event->mail }}">
                                    <img src="{{ asset('img/ico_email.png') }}" alt="icône email">
                                </a>
                            @endif
                        </div>
                        @if(!empty($event->presentation_video))
                            <div class="row">
                                <div class="large-12 columns small-12">
                                    <div class='video'>
                                        <iframe data-type="" width="400" height="250"
                                                src="{{ $event->presentation_video_iframe }}"
                                                title="video">
                                        </iframe>
                                    </div>
                                </div>
                            </div>
                        @elseif(!empty($event->presentation_picture))
                            <div class="row">
                                <div class="large-12 columns small-12">
                                    <img src="{{ asset('img/pictures/normal/'.$event->presentation_picture) }}"
                                         alt="presentation_picture">
                                </div>
                            </div>
                        @endif
                    </div>

                    @if(!empty($event->description))
                        <div class="row">
                            <div class="large-12 columns small-12">
                                <p><img src="{{ asset('img/ico_description.png') }}" alt="icône description">
                                    Description
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns small-12">
                                <p>{{$event->description or 'Default'}}</p>
                            </div>
                        </div>
                    @endif
                    <a href="#" data-reveal-id="addAlert" class="button button-small">Recevoir une alerte</a>
                    <hr/>
                </div>
            </div>

        </section>
    </main>
    <meta name="csrf-token" content="{{ csrf_token() }}" property=""/>
    <input type="hidden" name="event_id" value="{{ $event->id }}">
    <div id="addAlert" class="reveal-modal tiny" data-reveal aria-hidden="true" role="dialog">
        <h2>Recevoir une alerte pour {{ $event->title }}</h2>
        <input type="number" name="days_before" placeholder="Nombre de jours avant" min="0" value="14">
        <a class="button button-small" id="addAlertButton" href="{{ route('alertes.store') }}">Valider</a>
        <p class="error-label" style="color:#ff9183; display:none">Oups, quelque chose s'est mal passé lors de l'envoi de l'alerte...</p>
        <p>Astuce : vous pouvez gérer vos alertes en allant sur <a href="{{ route('alertes.index') }}">cette page</a></p>
        <a class="close-reveal-modal" aria-label="Fermer">&#215;</a>
    </div>
    @if(\Carbon\Carbon::today() > $event->end_date)
        <div id="addOpinion" class="reveal-modal tiny" data-reveal aria-hidden="true" role="dialog">
            <h2>Noter l'événement</h2>
            <div class="row collapse">
                <div class="columns small-4 small-offset-2">
                    <input type="number" name="note" placeholder="Note" min="0" max="6" value="0">
                </div>
                <div class="columns small-4">
                    <span class="postfix">/ 6</span>
                </div>
                <div class="columns small-3 hidden"></div>
            </div>
            <div class="text-center">
                <a class="button button-small" id="addOpinionButton" href="{{ route('opinion.store') }}">Valider</a>
            </div>
            <p class="error-label" style="color:#ff9183; display:none">Oups, quelque chose s'est mal passé lors de l'envoi de l'alerte...</p>
            <a class="close-reveal-modal" aria-label="Fermer">&#215;</a>
        </div>
    @endif
@endsection

@section('scripts')
    <script type="application/javascript" async>
        var csrf_token = $('[name="csrf-token"]').attr('content');
        var errorLabel =$("#addAlert .error-label");
        $("#addAlertButton").click(function () {
            var settings = {
                url: $(this).attr("href"),
                type: 'POST',
                data: {
                    _token: csrf_token,
                    event_id: $("[name='event_id']").val(),
                    days_before: $("[name='days_before']").val()
                },
                dataType: 'JSON',
                success: function () {
                    errorLabel.fadeOut(150);
                    $('#addAlert > a.close-reveal-modal').trigger('click');
                },
                error: function() {
                    errorLabel.fadeIn(150);

                }
            };
            $.ajax(settings);
            return false;
        });
        $("#addOpinionButton").click(function () {
            var errorLabel =$("#addOpinion .error-label");
            var settings = {
                url: $(this).attr("href"),
                type: 'POST',
                data: {
                    _token: csrf_token,
                    event_id: $("[name='event_id']").val(),
                    note: $("[name='note']").val()
                },
                success: function () {
                    errorLabel.fadeOut(150);
                    $('#addOpinion > a.close-reveal-modal').trigger('click');
                },
                error: function(response) {
                    errorLabel.fadeIn(150);
                    console.log(response.responseText);
                }
            };
            $.ajax(settings);
            return false;
        });
    </script>
@endsection