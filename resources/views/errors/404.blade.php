@extends('master')

@section('title')
    Agenda
@endsection

@section('content')

	<div class="row">
		<div class="small-12 small-centered medium-12 medium-centered large-12 large-centered columns">
			<img src="{{ asset ('img/sad404.png') }}" alt="image 404">
		</div>
        <div class="text-center columns">
            <a href="{{ url("/") }}">Retourner à la page d'accueil</a>
        </div>
	</div>

@endsection
