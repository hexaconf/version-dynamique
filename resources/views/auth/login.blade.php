@extends('master')

@section('title')
    Connexion
@endsection

@section('content')

    <main>
        <div class="row">
            <div class="small-12 medium-8 medium-centered large-8 large-centered columns">

                @include('auth.partial._login_form')

            </div>
        </div>
    </main>
@endsection