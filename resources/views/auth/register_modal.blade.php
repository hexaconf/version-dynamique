<div id="register-modal" class="reveal-modal auth-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true"
     role="dialog">
    <a class="close-reveal-modal" aria-label="Fermer">&#215;</a>


    <form class="panel" method="post" action="{{ url('/auth/inscription') }}">
        {!! csrf_field() !!}
        <h1>Inscription</h1>

        <p>
            En vous inscrivant gratuitement, vous aurez la possibilité de noter les événements auxquels vous
            avez assisté, et vous pourrez également recevoir des alertes par mail pour ceux qui vous
            intéressent. Venez vite nous rejoindre !
        </p>

        @if(count($errors) > 0)
            <div data-alert class="alert-box alert">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
                <a href="#" class="close">&times;</a>
            </div>
        @endif
        <p>
            <b>Nom * :</b>
            <input type="text" placeholder="Nom" name="lastname" required>
        </p>

        <p>
            <b>Prénom :</b>
            <input type="text" placeholder="Prénom" name="firstname">
        </p>

        <p>
            <b>Adresse e-mail * :</b>
            <input type="text" placeholder="Prénom" name="email" required>
        </p>

        <p>
            <b>Choisissez un identifiant * :</b>
            <input type="text" placeholder="Identifiant" name="login" required>
        </p>

        <p>
            <b>Mot de passe * :</b>
            <input type="password" placeholder="Mot de passe" name="password" required>
        </p>

        <p>
            <b>Confirmer le mot de passe * :</b>
            <input type="password" placeholder="Mot de passe" name="password_confirmation" required>
        </p>

        <p>
            <small>* : Champ obligatoire</small>
        </p>
        <div class="text-center">
            <input type="submit" value="Valider l'inscription" class="button"/>
        </div>
        <p class="text-center">
            Déjà enregistré ? <a data-reveal-id="connect-modal">Connectez-vous</a> maintenant !
        </p>
    </form>
</div>
