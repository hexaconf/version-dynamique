<div id="connect-modal" class="reveal-modal auth-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true"
     role="dialog">
    <a class="close-reveal-modal" aria-label="Fermer">&#215;</a>

    @include('auth.partial._login_form')

</div>
