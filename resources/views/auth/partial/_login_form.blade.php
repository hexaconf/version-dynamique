<form class="panel" method="POST" action="{{ url('/auth/connexion') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

    <h1>Connexion</h1>
    @if(count($errors) > 0)
        <div data-alert class="alert-box alert round">
            @foreach($errors->all() as $error)
                <p>{!! $error !!}</p>
            @endforeach
            <a href="#" class="close">&times;</a>
        </div>
    @endif

    <p>
        <label for="login">Identifiant ou adresse e-mail :</label>
        <input type="text" name="login" id="login" required>
    </p>

    <p>
        <label for="password">Mot de passe :</label>
        <input type="password" name="password" id="password" required>
    </p>
    <input type="checkbox" name="remember" id="remember">
    <label for="remember">Se souvenir de moi</label>

    <p>
        Vous n'avez pas de compte ? <a href="{{ url('/auth/inscription') }}">Inscrivez-vous</a>
        gratuitement !
    </p>

    <div class="text-center">
        <input type="submit" value="Se connecter" class="button"/>
    </div>
</form>