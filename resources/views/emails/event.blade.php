<table style="margin: 0 10% 15px 10%; border-spacing:5px;">
    <caption>
        <h2 style="font-weight: bold;color: #056687;font-family:'Lucida Sans',sans-serif">{{ $event->title }}</h2>
    </caption>
    <tbody>
    <tr>
        <td style="width: 50%" valign="top">
            {{ $event->short_description }}
        </td>
        <td valign="top">
            <ul style="margin: 0; list-style-type: none">
                <li style="margin: 3px 0"><img style="vertical-align: middle" src="{{ asset('img/ico_calendrier.png') }}" alt="Icone calendrier"/>Du {{ $event->begin_date }} au {{ $event->end_date }}</li>
                <li><img style="vertical-align: middle" src="{{ asset('img/ico_localisation.png') }}" alt="Icone localisation"/>{{ $event->address }}</li>
            </ul>
        </td>
    </tr>
    <tr align="center">
        <td colspan="2" style="padding-top:30px">
            <a style="padding:10px;background-color:#056687;color:white;text-decoration:none;font-size:14pt"  href="{{ url("evenements/".$event->id) }}">Voir sur Hexaconf.fr</a>
        </td>
    </tr>
    </tbody>
</table>