<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
        body {
            font-family: Arial, sans-serif;
            background-color: #f6f6f6;
        }
        h1, h2 {
            font-family: 'Lucida Sans', sans-serif;
        }
        thead {
            text-align: center;
        }
        tfoot {
            font-size: small;
            color: #a3a3a3;
            text-align: center;
        }
        .tabulation {
            text-indent: 2em;
        }
    </style>
</head>
<body>
<table style="padding:10px;margin:0 5%;background-color:white;border:solid 1px #aeaeae;">
    <thead>
    <tr>
        <td style="float: left;">
            <img src="{{asset('img/logo-hexaconf-small.png')}}" alt="Logo Hexaconf">
        </td>
    </tr>
    <tr>
        <td align="center">

            <h1 style="display:inline;font-weight:bold;color:#056687;font-family:'Lucida Sans',sans-serif;">
                {{ $alert->event->title }} arrive bientôt !
            </h1>
        </td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td valign="top">
            <p>Bonjour {{ $alert->user->fullname }},</p>
            <p>
                Nous avons le plaisir de vous rappeler que dans {{ $alert->days_before }} jours se déroulera l'événement <a href="{{ url('/evenements/'.$alert->event->id) }}" style="color: #056687">{{ $alert->event->title }}</a>. Si vous ne l'avez pas encore fait, réservez vos places au plus vite !
                <br><br>
                À bientôt sur <a href="{{ url('/') }}" style="color: #056687">Hexaconf.fr</a> !
            </p>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <div style="border: solid 1px #c4c4c4">
                @include('emails.event', ['event'=>$alert->event])
            </div>

        </td>
    </tr>
    </tbody>
    <tfoot style="margin-top: 15px">
    <tr>
        <td>
            Vous avez reçu ce mail car vous avez demandé à recevoir une alerte pour cet événement.
        </td>
    </tr>
    <tr>
        <td><a href="#" style="color:#797979">Se désinscrire de toutes les alertes</a></td>
    </tr>
    </tfoot>
</table>
</body>
</html>