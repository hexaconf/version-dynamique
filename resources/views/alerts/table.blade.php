<table class="table-hover center-block center" id="tableAlerts">
    <thead>
    <tr>
        <td>Événement</td>
        <td>Date</td>
        <td></td>
    </tr>
    </thead>
    <tbody id="alerts">
    @foreach($alerts as $alert)
        <tr>
            <td>{{ $alert->event->title }}</td>
            <td>{{ $alert->date }} ({{ $alert->days_before }} jours avant)</td>
            <td>
                {!! Form::open(['route' => ['alertes.destroy', $alert->id], 'method' => 'delete']) !!}
                    {{ csrf_field() }}
                    <button type="submit" class="button alert">Supprimer</button>
                
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    <tr id="rowAddHidden" hidden>
        <td>
            <select name="event_id">
                <option disabled selected>Sélectionner un événement</option>
                @foreach(\App\Event::actives() as $event)
                    <option value="{{$event->id}}">{{$event->title}}</option>
                @endforeach
            </select>
        </td>
        <td>
            <input type="number" name="days_before" placeholder="Nombre de jours avant">
        </td>
        <td>
            <a class="button button-small alert-add" href="{{ route('alertes.store') }}">Ajouter</a>
            <a class="button button-small alert-cancel" href="#">Annuler</a>
        </td>
    </tr>
    </tbody>
    <tfoot>
    <tr id="newAlert">
        <td colspan="4">
            <a href="#" id="addAlert" class="button button-small">Nouvelle alerte</a>
        </td>
    </tr>
    </tfoot>
</table>
<meta name="csrf-token" content="{{ csrf_token() }}" property=""/>