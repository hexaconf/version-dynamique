@extends('master')

@section('title')
    Vos alertes
@endsection

@section('content')
    <main>
        <div class="row">
            <div class="columns small-12 center-block center float-none">
                @include('alerts.table')
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script type="application/javascript" src="{{ asset('js/alerts.js') }}"></script>
@endsection