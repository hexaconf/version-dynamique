@extends('master')

@section('title')
    Nous contacter
@endsection

@section('content')

    <main>
        <div class="row">
            <div class="small-12 medium-12 large-8 columns">
                <div class="panel">
                    <div class="row">
                        <div class="columns">
                            <h2>Contactez-nous via ce formulaire</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="columns">
                            <div class="row">
                                <div class="small-6 medium-6 large-6 columns small-no-padding-left">
                                    <label for="nomContact"><b>Nom* :</b></label>
                                    <input id="nomContact" type="text" placeholder="Nom" required></p>
                                </div>
                                <div class="small-6 medium-6 large-6 columns small-no-padding-right">
                                    <label for="prenomContact"><b>Prénom* : </b></label>
                                    <input id="prenomContact" type="text" placeholder="Prenom" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="columns small-no-padding">
                                    <label for="emailContact"><b>E-Mail* :</b></label>
                                    <input id="emailContact" type="email" placeholder="E-mail" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="columns small-no-padding">
                                    <label for="messageContact"><b>Message* :</b></label>
                                    <textarea id="messageContact" placeholder="Ecrivez ici votre message" rows="10" required></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="columns">
                                    <input type="button" class="button" id="envoyerContact" value="Envoyer">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="small-12 medium-12 large-4 columns">
                <div class="panel">
                    <div class="small-12 small-centered medium-12 medium-centered large-12 large-centered columns">
                        <h2>Ou bien retrouvez nous sur place !</h2>
                        <p>IUT de Bordeaux</p>
                        <p>1 Rue de Naudet</p>
                        <p>33175 GRADIGNAN</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
