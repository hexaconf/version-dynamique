<!doctype html>
<html class="no-js" lang="fr">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.tagit.css') }}">
    <link rel="stylesheet" href="{{ asset('css/foundation-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tagit.ui-zendesk.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    @yield('css')
    <title>@yield('title', 'Page') | Hexaconf</title>
</head>

<body>
<header>
    @include('header')
</header>

@yield('content')

@include('auth/login_modal')
@include('auth/register_modal')

<footer>
    @include('footer')
</footer>

<a href="#" class="buttonGoTop"></a>

<script src="{{ asset('js/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('js/vendor/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/tag-it.min.js') }}"></script>
<script src="{{ asset('js/foundation.min.js') }}"></script>
<script src="{{ asset('js/foundation/foundation.equalizer.js') }}"></script>
<script src="{{ asset('js/foundation/foundation.interchange.js') }}"></script>
<script src="{{ asset('js/foundation/foundation-datepicker.min.js') }}"></script>
<script src="{{ asset('js/vendor/modernizr.js') }}"></script>
<script src="{{ asset('js/init.js') }}"></script>
@yield('scripts')
</body>

</html>