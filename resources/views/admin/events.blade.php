@extends('admin/master')

@section('content')


    <div id="content">
        <h2>Evenements</h2>
        @foreach($events as $event)
            <div class="event">
                <h3 class="title">{{ $event->title }}</h3>

                <div class="buttons">
                    <a href="{{ url('/evenements/validates/' . $event->id )}}"
                       class="@if($event->valid ) success @endif button radius validate">
                        @if($event->valid == 1)
                            Valide
                        @else
                            Valider
                        @endif
                    </a>
                    <a href="{{ url('/evenements/' . $event->id )}}" class="button radius">
                        Regarder
                    </a>
                    <a href="{{ url('/evenements/' . $event->id . '/modifier' )}}" class="button radius">
                        Modifier
                    </a>
                </div>
            </div>
        @endforeach
    </div>

@endsection