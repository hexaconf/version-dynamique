@extends('admin/master')

@section('content')

    <div id="content">
        <h2>Utilisateurs</h2>
        @foreach($users as $user)
            <div class="user">
                <h3 class="name">{{ $user->name() }}</h3>
                <p>Status : {{ $user->status() }}</p>
                <div>
                    <form action="{{ url('/account/'. $user->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button>Supprimer</button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>

@endsection