<!doctype html>
<html class="no-js" lang="fr">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation.min.css') }}"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backoffice.css') }}">
    @yield('css')
    <title>@yield('title', 'Page') | Administration</title>
</head>

<body>
<header>
    <nav class="top-bar" data-topbar role="navigation">
        <div class="top-bar-section">
            <ul class="right">
                <li class="active"><a href="{{ url('/account') }}">{{ Auth::user()->name() }}</a></li>
                <li class="has-dropdown">
                    <a href="#">Mon compte</a>
                    <ul class="dropdown">
                        <li class="active red"><a href="{{ url('/auth/deconnexion') }}">Me déconnecter</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>
</header>

<div id="menu-left">
    <ul class="mobile-nav">
        <li class="mobile-nav-item" id="events"><a href="{{ url('/admin/events') }}">Évènements</a></li>
        <li class="mobile-nav-item" id="users"><a href="{{ url('/admin/users') }}">Utilisateurs</a></li>
    </ul>
</div>

@yield('content')

<footer>
</footer>

<a href="#" class="buttonGoTop"></a>

<script src="{{ asset('js/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('js/foundation.min.js') }}"></script>
<script src="{{ asset('js/vendor/modernizr.js') }}"></script>
<script src="{{ asset('js/backoffice.js') }}"></script>
</body>

</html>