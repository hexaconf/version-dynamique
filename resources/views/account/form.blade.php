@extends('master')

@section('title')
    Manager son compte
@endsection

@section('content')

    <main>
        <div class="row">

            <h1>Mon compte</h1>

            <div class="small-12 large-4 columns" id="manage-menu">
                <ul id="onglets-manage-menu">
                    <li><a href="#"> Mon compte </a></li>
                    <li><a href="{{ route('alertes.index') }}" target="_blank"> Mes alertes </a></li>
                    <li><a href="#"> Mes conférences </a></li>
                </ul>
            </div>

            <div class="small-12 large-8 columns">

                @if(count($errors) > 0)
                    <div data-alert class="alert-box alert round">
                        @foreach($errors->all() as $error)
                            {!! $error !!}
                        @endforeach
                        <a href="#" class="close">&times;</a>
                    </div>
                @endif

                {!! Form::open(array('action' => 'AccountController@update')) !!}

                <div class="row">
                    <div class="medium-4 large-5 columns">Prénom</div>
                    <div class="medium-8 large-7 columns">
                        {!! Form::text('firstname', old('firstname', $user->firstname), array('required' => 'required', 'placeholder' => 'Prénom')) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="medium-4 large-5 columns">Nom</div>
                    <div class="medium-8 large-7 columns">
                        {!! Form::text('lastname', old('lastname', $user->lastname), array('required' => 'required', 'placeholder' => 'Nom')) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="medium-4 large-5 columns">Adresse email</div>
                    <div class="medium-8 large-7 columns">
                        {!! Form::text('email', old('email', $user->email), array('required' => 'required')) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="medium-4 large-5 columns">Identifiant</div>
                    <div class="medium-8 large-7 columns">
                        {!! Form::text('login', old('login', $user->login), array('readonly')) !!}
                    </div>
                </div>

                <div class="row margin-top">
                    <div class="large-8 small-centered large-centered columns">
                        <h3>Modifier votre mot de passe</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="large-5 columns">Nouveau mot de passe</div>
                    <div class="large-7 columns">
                        {!! Form::password('new_password') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="large-5 columns">Confirmer nouveau mot de passe</div>
                    <div class="large-7 columns">
                        {!! Form::password('new_password_confirm') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-centered large-3 large-centered columns">
                        <button type="submit">
                            Enregistrer
                        </button>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </main>
@endsection
