var map;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5
    });
    addMarkers();
}
function addMarker(lat, long, text, link, map) {
    var marker = new google.maps.Marker({
        position: {
            lat: lat,
            lng: long
        },
        map: map
    });
    attachInfoWindow(marker, text, link)
}
function addMarkers() {
    if (urlCoords) {
        $.getJSON(urlCoords, function(eventsCoordinates) {
            $(eventsCoordinates).each(function(index){
                addMarker(eventsCoordinates[index].lat, eventsCoordinates[index].lng, eventsCoordinates[index].title, eventsCoordinates[index].url, map);
            });
        });
    }
}
function attachInfoWindow(marker, text, link) {
    var resultingText;
    if (typeof link != 'undefined') {
        resultingText = "<a href='"+link+"' target='_blank'>"+text+"</a>"
    }
    else resultingText = text;
    var infowindow = new google.maps.InfoWindow({
        content: resultingText
    });
    marker.addListener('click', function() {
        infowindow.open(marker.get('map'), marker);
    });
}

function resizeMap() {
    google.maps.event.trigger(map, 'resize');
    map.setCenter(new google.maps.LatLng(46.5967, 2.8733205));
}