// Alerts
$("#addAlert").click(function (event) {
    event.preventDefault();
    var rowAdd = $("#rowAddHidden").clone().removeAttr('id');
    $("#alerts").append(rowAdd);
    rowAdd.fadeIn();
    $("#newAlert").hide();
    registerAddAlertClick();
    registerCancelClick();
});

var csrf_token = $('[name="csrf-token"]').attr('content');
function registerAddAlertClick() {
    $(".alert-add").click(function (event) {
        event.preventDefault();

        var currentRow = $(this).parent().parent();
        var settings = {
            url: $(this).attr("href"),
            type: 'POST',
            data: {
                _token: csrf_token,
                event_id: currentRow.find("select[name='event_id']").val(),
                days_before: currentRow.find("input[name='days_before']").val()
            },
            dataType: 'JSON',
            success: function (newAlert) {
                $("#newAlert").show();
                currentRow.find("select[name='event_id']").replaceWith(newAlert.event);
                currentRow.find("input[name='days_before']")
                    .replaceWith(newAlert.date + " (" + newAlert.days_before + " jours avant)");
                currentRow.find(".alert-cancel").remove();
                var buttonDelete = $("<a class='button button-small button-delete' href='" + newAlert.delete_route + "'>Supprimer</a>")
                buttonDelete.click(buttonDeleteClick);
                currentRow.find(".alert-add").replaceWith(buttonDelete);

            },
            error: function (data) {
                alert('Oups, quelque chose s\'est mal passé lors de l\'ajout...');
                console.log(data.responseText);
            }
        };
        $.ajax(settings);
    });
}

function registerCancelClick() {
    $(".alert-cancel").click(function (event) {
        event.preventDefault();
        showAgainNewAlertButton.call(this);
    });
}

function showAgainNewAlertButton() {
    $(this).parent().parent().remove();
    $("#newAlert").show();
}

function buttonDeleteClick() {
    event.preventDefault();
    var currentRow = $(this).parent().parent();
    if (window.confirm('Êtes-vous sûr(e) de vouloir supprimer cette alerte ?')) {
        $.ajax({
            url: $(this).attr("href"),
            type: 'DELETE',
            data: {
                _token: csrf_token
            },
            dataType: 'text',
            success: function () {
                currentRow.remove();
            },
            error: function (data) {
                alert('Oups, quelque chose s\'est mal passé lors de la suppression...');
                console.log(data.responseText);
            }
        });
    }
}

$(".button-delete").click(buttonDeleteClick);