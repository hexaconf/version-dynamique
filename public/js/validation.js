$(function () {

    // Custom add field for
    $('#add-price-field').click(function () {
        var last_field = $("#newPrice");
        last_field.clone().removeAttr("id")
            .appendTo(".price-fields > .fields")
            .fadeIn(300).find('.delete-price').click(deletePrice);
    });
    $(".delete-price").click(deletePrice);
    function deletePrice() {
        $(this).parent().parent().remove();
        return false;
    }

    // Custom datepicker
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#begin_date').fdatepicker({
        format: 'yyyy-mm-dd',
        onRender: function (date) {
            return date.valueOf() <= now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate());
            checkout.update(newDate);
        }
        checkin.hide();
        $('#end_date')[0].focus();
    }).data('datepicker');

    var checkout = $('#end_date').fdatepicker({
        format: 'yyyy-mm-dd',
        onRender: function (date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function () {
        checkout.hide();
    }).data('datepicker');

    // Used for tags on event form
    $("#singleFieldTags").tagit({
        singleField: true,
        singleFieldNode: $('#mySingleField')
    });

    var titles;

    $.ajax("/evenements/titles").done(function (titlesJson) {
        titles = titlesJson;
    });

    title = $('input[name=title]');

    title.focus(function () {
        if (titles.indexOf(title.val()) != -1) {
            title.addClass("wrong");
        } else {
            title.removeClass("wrong");
        }
    });

    var subforms = $("#form").find("> form > .row");

    subforms.hide();
    $(subforms.get(0)).show();
    var index = 0;

    subforms.find(".next").click(function () {
        if (valid(index)) {
            $(subforms.get(index)).slideUp();
            $(subforms.get(++index)).slideToggle();
        }
    });

    subforms.find(".previous").click(function () {
        $(subforms.get(index)).slideUp();
        $(subforms.get(--index)).slideToggle();
    });

    var form = $(subforms.get(index));

    function valid(index) {
        var nbErrors = 0;
        if (index == 0) {
            var title = form.find("*[name=title]").val();
            if (title == "") {
                addError("title", "Le titre est obligatoire");
                nbErrors++;
            }
            //if (titles.get(title) != 0) {
            //    addError("title", "Ce titre est déjà pris");
            //}

        }

        return (nbErrors == 0);
    }

    function addError(key, message) {
        form.find($('#' + key + "-error")).append(document.createTextNode(message));
    }


});