$(function () {
    var resized = false;
    $(document).foundation();

    $(document).on('opened.fndtn.reveal', '#mapModal', function () {
        if (!resized) {
            resizeMap();
            resized = true;
        }
    });

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68966290-1', 'auto');
    ga('send', 'pageview');

    // Custom onclick function for large layouts of events
    $('*[data-href]').click(function () {
        window.location = $(this).attr('data-href');
    });

    //Back to top button
    $(window).scroll(onScroll);
    function onScroll() {
        if ($(this).scrollTop() < 40) {
            $("a.buttonGoTop").fadeOut();
        } else {
            $("a.buttonGoTop").fadeIn();
        }
    }
    $("a.buttonGoTop").click(function () {
        // stop the scroll event handling
        $(window).off('scroll');
        $('html, body').animate({scrollTop: 0}, 'slow', function(){
            $(window).scroll(onScroll);
        });
    });

    // Search button
    $(".search-input").focus(function () {
        if ($(this).val() == "") {
            $(this).data('oldWidth', $(this).css("width")).animate({width: "92px"}, 200).addClass("focused");
        }
    }).blur(function () {
        if ($(this).val() == "") {
            $(this).animate({width: $(this).data('oldWidth')}, 200).removeClass('focused').removeAttr('style');
        }
    });
});
