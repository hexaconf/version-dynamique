// Event handler for agenda
$(function () {

    // agenda select days, years and month bar
    var agenda = $("#agenda");
    var search = "";
    var grid = $(".grid");

    agenda.children().hide();
    agenda.children(":first").show();
    agenda.children().hover().css("cursor", "pointer");

    agenda.find('#remove').click(function () {
        search = "";
        agenda.children(":first").slideDown();
        agenda.children(":last").hide();
        grid.children().fadeIn();
    });

    agenda.find('p').click(function () {
        search = "." + $(this).text();
        grid.children().not(search).fadeOut();
    });

    agenda.find('> .row').not('#agenda > .row:last').click(function () {
        $(this).next().slideDown();
        $(this).slideUp();
    });

});