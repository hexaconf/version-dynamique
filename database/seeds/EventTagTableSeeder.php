<?php

use Illuminate\Database\Seeder;

class EventTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_tag')->insert([
            'event_id' => "1",
            'tag_id' => "1",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('event_tag')->insert([
            'event_id' => "1",
            'tag_id' => "2",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('event_tag')->insert([
            'event_id' => "1",
            'tag_id' => "3",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('event_tag')->insert([
            'event_id' => "1",
            'tag_id' => "5",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('event_tag')->insert([
            'event_id' => "2",
            'tag_id' => "1",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('event_tag')->insert([
            'event_id' => "2",
            'tag_id' => "5",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
