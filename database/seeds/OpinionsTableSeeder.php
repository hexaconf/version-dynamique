<?php

use Illuminate\Database\Seeder;

class OpinionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('opinions')->insert(
            [
                'score' => "3",
                'comment' => "Bon intervenants, journée un peu trop techy pour moi, les petits fours etaient plutôt bien.",
                'event_id' => "1",
                'user_id' => "3",
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]);

        DB::table('opinions')->insert([
            'score' => "4",
            'comment' => "Excellent evenements, très bons intervenants",
            'event_id' => "1",
            'user_id' => "2",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        DB::table('opinions')->insert([
                'score' => "4",
                'comment' => "Bon week-end de conférences, des conférenciers au top !",
                'event_id' => "2",
                'user_id' => "1",
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ]
        );
    }
}
