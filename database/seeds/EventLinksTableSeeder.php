<?php

use Illuminate\Database\Seeder;

class EventLinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_links')->insert([
            'value' => "youtube.com/unlienversyoutube",
            'event_id' => "2",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('event_links')->insert([
            'value' => "youtube.com/unautrelienversyoutube",
            'event_id' => "1",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('event_links')->insert([
            'value' => "random.com/lienrandom",
            'event_id' => "1",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('event_links')->insert([
            'value' => "twitter.com/BDXIO",
            'event_id' => "1",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
