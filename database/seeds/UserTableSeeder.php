<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => "Administrateur",
            'lastname' => "Admin",
            'login' => "admin",
            'email' => "hexaconf@gmail.com",
            'password' => bcrypt("password_secured"),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('users')->insert([
            'firstname' => "Jean",
            'lastname' => "Laroche",
            'login' => "jlr",
            'email' => "jlr@gmail.com",
            'password' => bcrypt("password_not_secured"),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('users')->insert([
            'firstname' => "a",
            'lastname' => "A",
            'login' => "a",
            'email' => "a@gmail.com",
            'password' => bcrypt("password"),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
