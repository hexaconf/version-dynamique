<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'title' => "français",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('languages')->insert([
            'title' => "anglais",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('languages')->insert([
            'title' => "espagnol",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
