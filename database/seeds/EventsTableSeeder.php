<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            'title' => "BDX I/O 2015",
            'begin_date' => "2015-10-16",
            'end_date' => "2015-10-16",
            'subject' => "Conférence du numérique sur Bordeaux",
            'begin_hour' => "8h30",
            'end_hour' => "19h",
            'description' => "BDX.IO est l’occasion de réunir les artisans de l’économie numérique girondine, de leur proposer des interventions de qualité dans un cadre sympathique et convivial. Nous souhaitons apporter notre pierre à l’édifice de l’écosystème bordelais et étendre son rayonnement en France.",
            'address' => " 1 Avenue Docteur Albert Schweitzer, 33400 Talence, France",
            'presentation_video' => "https://www.youtube.com/watch?v=yP8ybzmVi_A",
            'presentation_picture' => "",
            'seating_capacity' => "500",
            'edition' => "1",
            'valid' => '1',
            'content_link' => "bdx.io",
            'user_id' => "1",
            'language_id' => "1",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('events')->insert([
            'title' => "Alea 2016",
            'begin_date' => "2016-05-07",
            'end_date' => "2016-05-11",
            'subject' => "Journées ALEA 2016",
            'begin_hour' => "8h",
            'end_hour' => "18h",
            'valid' => '1',
            'description' => "
                Le groupe Aléa s'intéresse aux structures aléatoires discrètes issues de diverses disciplines : l'informatique théorique, les mathématiques discrètes, la théorie des probabilités, la physique statistique, la bio-informatique.
                La principale manifestation organisée par le groupe Aléa est un rendez-vous annuel, les journées ALEA. A ce titre, il s'agit d'un évènement important et structurant pour la communauté.Lire la suite
                La semaine est structurée autour de trois cours avancés de 2h30, chacun étant complété par une séance d'exercices de 1h, dont l'expérience montre qu'il s'agit de compléments essentiels.
                Pour l'année 2016, le programme envisagé pour les cours s'articule autour des trois thèmes suivants:
                    méthodes d'analyse pour les algorithmes diviser-pour-régner
                    convergences en théorie des probabilités
                    combinatoire des déterminants
                Ce programme recouvre une large part des thèmes du groupe ALEA : combinatoire énumérative et bijective, probabilités, interactions entre physique et combinatoire et leurs aspects algorithmiques. Quelques exposés \"longs\" sont également prévus, permettant de donner un panorama des différentes thématiques de recherche. Enfin, des communications plus brèves permetteront aux doctorants d'exposer leur travail.",
            'address' => "Marseille, France",
            'seating_capacity' => "25",
            'edition' => "2",
            'user_id' => "2",
            'language_id' => "1",
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
