<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(EventLinksTableSeeder::class);
        $this->call(EventTagTableSeeder::class);
        $this->call(OpinionsTableSeeder::class);
        $this->call(AlertsTableSeeder::class);

        Model::reguard();

    }
}
