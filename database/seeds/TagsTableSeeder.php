<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'title' => "Web",
            'active' => 'true',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('tags')->insert([
            'title' => "Programmation",
            'active' => 'true',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('tags')->insert([
            'title' => "Laravel",
            'active' => 'true',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('tags')->insert([
            'title' => "IT",
            'active' => 'true',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('tags')->insert([
            'title' => "Recherche",
            'active' => 'true',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        DB::table('tags')->insert([
            'title' => "Intelligence Artificielle",
            'active' => 'true',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
