<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('title')->unique();
            $table->string('subject')->required();
            $table->date('begin_date');
            $table->date('end_date');
            $table->string('begin_hour');
            $table->string('end_hour');
            $table->longText('description');
            $table->string('address');
            $table->string('presentation_video');
            $table->string('presentation_picture');
            $table->integer('seating_capacity');
            $table->integer('edition');
            $table->string('content_link');
            $table->string('website');
            $table->string('mail');
            $table->boolean('valid')->default(false);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('language_id');

            $table->timestamps();


        });

        Schema::table('events', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('CASCADE');;
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('CASCADE')->onUpdate('CASCADE');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
