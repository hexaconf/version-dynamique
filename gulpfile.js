var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
 
gulp.task('default', function() {
    return gulp.src('dev/*/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dev/.'));
});

gulp.task('minify-html', function() {
    return gulp.src('dev/*/*.html')
	    .pipe(htmlmin({collapseWhitespace: true}))
	    .pipe(gulp.dest('dev/.'))
});

gulp.task('minify-css', function() {
  return gulp.src('dev/*/*.css')
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('dev/.'));
});

gulp.task('uglify', function() {
  return gulp.src('dev/*/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dev/.'));
});

