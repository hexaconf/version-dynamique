<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTag extends Model
{
    protected $fillable = ['event_id', 'tag_id'];

    public function event()
    {
        return $this->hasOne('App\Event');
    }

    public function tag()
    {
        return $this->hasOne('App\Tag');
    }
}
