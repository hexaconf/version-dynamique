<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['title', 'begin_date', 'end_date', 'subject',
        'begin_hour', 'end_hour', 'description', 'address',
        'presentation_video', 'presentation_picture', 'seating_capacity',
        'edition', 'content_link', 'language_id', 'website',
        'facebook_link', 'twitter_link', 'youtube_link'];

    protected $guarded = ['valid'];

    public static $validator = ['title' => 'required|unique:events|max:55',
        'address' => 'required',
        'subject' => 'required',
        'begin_date' => 'required|date',
        'end_date' => 'required|date',
        'content_link' => 'url',
        'website' => 'url',
        'facebook_link' => 'url',
        'twitter_link' => 'url',
        'youtube_link' => 'url',
        'mail' => 'email',
    ];
    public static $validatorUpdate;

    /**
     * Event constructor.
     */
    public function __construct()
    {
        parent::__construct();
        self::$validatorUpdate = self::$validator;
        self::$validatorUpdate['title'] = 'required|max:55';
    }

    public static function withTag($tagSearched)
    {
        $allEvents = self::all();
        $matchingEvents = new Collection;
        foreach($allEvents as $event) {
            if ($event->tags->contains('title', $tagSearched)) {
                $matchingEvents->add($event);
            }
        }
        return $matchingEvents;
    }



    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    public function prices()
    {
        return $this->hasMany('App\Price');
    }

    public function links()
    {
        return $this->hasMany('App\EventLink');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function language()
    {
        return $this->belongsTo('App\Language');
    }

    /**
     * Gets all the opinions of the event
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function opinion()
    {
        return $this->hasMany('App\Opinion');
    }

    public static function getNext()
    {
        $nextDate = Event::where('begin_date', '>', Carbon::today()->toDateString())->where('valid', '1')->min('begin_date');
        return Event::where('begin_date', $nextDate)->first();
    }

    /**
     * Gets the current best evenement (with the best average note)
     * @return \App\Event
     */
    public static function getBest()
    {
        $eventsNotesArray = Event::actives()->pluck('average_note', 'id')->toArray();
        $bestEventId = array_keys($eventsNotesArray, max($eventsNotesArray))[0];

        return Event::find($bestEventId);
    }

    public static function actives()
    {
        return Event::where('valid', 1)->get();
    }

    /**
     * Gets the average note of an event, and returns this note or -1 if there is no note
     * @return float|int
     */
    public function getAverageNoteAttribute()
    {
        $sum = 0;
        $opinionList = $this->opinion;
        $opinionSize = 0;
        if ($opinionList !== null) {
            foreach ($opinionList as $opinion) {
                $sum += $opinion->score;
                $opinionSize++;
            }
            if ($opinionSize > 0) {
                $mid = $sum / $opinionSize;
                return round($mid, 1);
            }
        }
        return -1;
    }

    public function getDateAsCSSClass()
    {
        $begin = $this->begin_date;
        $end = $this->end_date;

        $begin = explode("-", $begin);
        $end = explode("-", $end);
        $class = "";

        $class .= intval($begin[0]) . " " . $this->intToMonth(intval($begin[1])) . " " . intval($begin[2]);

        if ($begin[2] != $end[2]) {
            $begin = intval($begin[2]);
            $diff = intval($end[2]) - $begin;
            for($i = $begin; $i <= $begin + $diff; $i++) {
                $class .= " " . $i ;
            }
        }

        return $class;

    }

    public function isOnManyDate()
    {
        return ($this->end_date != null && $this->end_date != $this->begin_date);
    }

    public function getShortDescriptionAttribute()
    {
        $descriptions = explode(" ", $this->description);
        $first_words = implode(" ", array_splice($descriptions, 0, 40));

        if (strlen(implode(" ", array_splice($descriptions, 40))) != 0)
            $first_words .= " ...";

        return $first_words;
    }

    private function intToMonth($value)
    {
        switch ($value) {
            case 1:
                return "Janvier";
            case 2:
                return "Fevrier";
            case 3:
                return "Mars";
            case 4:
                return "Avril";
            case 5:
                return "Mai";
            case 6:
                return "Juin";
            case 7:
                return "Juillet";
            case 8:
                return "Août";
            case 9:
                return "Septembre";
            case 10:
                return "Octobre";
            case 11:
                return "Novembre";
            case 12:
                return "Décembre";
            default:
                return "Error";
        }
    }

    public function getBeginDateCarbonAttribute()
    {
        return Carbon::parse($this->begin_date);
    }
    public function getEndDateCarbonAttribute()
    {
        return Carbon::parse($this->end_date);
    }

    public function getDatesForHumans()
    {
        if ($this->begin_date != $this->end_date) {
            return $this->begin_date_carbon->formatLocalized("%d %B %Y").' au '.$this->end_date_carbon->formatLocalized("%d %B %Y");
        }
        else {
            return $this->begin_date_carbon->formatLocalized("%d %B %Y");
        }
    }

    public static function getFromQuery($query)
    {
        return Event::where('title', 'LIKE', '%'.$query.'%')
            ->orWhere('description', 'LIKE', '%'.$query.'%')
            ->orWhere('address', 'LIKE', '%'.$query.'%')
            ->get();
    }

    public static function getFromParameters($title, $date, $address)
    {
        $events = Event::where('valid', '1');

        if (!empty($title)) {
            $events = $events->where('title', 'LIKE', '%'.$title.'%');
        }
        if (!empty($date)) {
            $events = $events->where('begin_date', '<=', $date)->where('end_date', '>=', $date);
        }
        if (!empty($address)) {
            $events = $events->where('address', 'LIKE', '%'.$address.'%');
        }
        return $events->get();
    }

    public function getPresentationVideoIframeAttribute()
    {
        return str_replace('watch?v=', 'v/', $this->presentation_video);
    }

    public function deletePrices()
    {
        foreach ($this->prices as $price) {
            $price->delete();
        }
    }

    public function deleteTags()
    {
        foreach ($this->prices as $price) {
            $price->delete();
        }
    }
}