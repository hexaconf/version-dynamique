<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class SetGlobalVariables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        setlocale(LC_TIME, 'fr_FR.utf8');

        return $next($request);
    }
}
