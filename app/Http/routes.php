<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('auth/connexion', 'Auth\AuthController@getLogin');
Route::post('auth/connexion', 'Auth\AuthController@postLogin');
Route::get('auth/deconnexion', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/inscription', 'Auth\AuthController@getRegister');
Route::post('auth/inscription', 'Auth\AuthController@postRegister');


// Users route
Route::get('/api/users', 'AccountController@getUsers');
Route::get('/api/evenements/titles', 'EventsController@getTitles');
Route::get('/api/evenements', 'EventsController@getEventsSortByStatus');

// Account routes...
Route::get('/account', 'AccountController@edit')->middleware('auth');
Route::post('/account/{user}', 'AccountController@update')->middleware('auth');
Route::delete('/account/{id}', 'AccountController@destroy')->middleware('auth');

Route::get('/', 'PagesController@index');

Route::get('/evenements', 'EventsController@index');
Route::get('/evenements/recherche_avancee', 'EventsController@advancedSearch');
Route::post('/evenements/rechercher', 'EventsController@search');
Route::get('/evenements/rechercher/tag/{tag}', 'EventsController@searchByTag');
Route::get('/evenements/titles', 'EventsController@getTitles');

Route::get('/evenements/agenda', 'EventsController@showAgenda');
Route::get('/evenements/ajouter', 'EventsController@create')->middleware('auth');
Route::get('/evenements/validates/{id}', 'EventsController@validatesEvent')->middleware('auth');
Route::post('/evenements/ajouter', 'EventsController@store')->name('events.store')->middleware('auth');
Route::get('/evenements/{id}', 'EventsController@show');
Route::get('/evenements/{id}/modifier', 'EventsController@edit')->middleware('auth');
Route::post('/evenements/{id}/modifier', 'EventsController@update')->middleware('auth')->name('events.update');
Route::get('/evenements/{id}/tags', 'EventsController@getTagsJson');

Route::get('/gpscoords', 'EventsController@eventsWithCoordinates');


Route::get('/admin', 'AdminController@index')->middleware('auth');
Route::get('/admin/users', 'AdminController@users')->middleware('auth');
Route::get('/admin/events', 'AdminController@events')->middleware('auth');

Route::get('/contact', 'PagesController@contact');
Route::get('/mentions-legales', 'PagesController@legalMentions');

/**
 * Security to avoid route crash for some defaults redirects
 */
Route::get('/home', function() {
    return redirect('/');
});

Route::get('/test_email', 'EmailTestController@index');

Route::resource('alertes', 'AlertsController', ['except' => ['show', 'create', 'update', 'edit']]);
Route::post('avis/add', 'OpinionsController@store')->name('opinion.store');
Route::get('alertes/table', 'AlertsController@getHtmlTableListingAlerts');

Route::get("/sitemap", function(){
    // create new sitemap object
    $sitemap = App::make("sitemap");

    // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
    // by default cache is disabled
    $sitemap->setCache('laravel.sitemap', 180);

    // check if there is cached sitemap and build new only if is not
    if (!$sitemap->isCached())
    {
        // add item to the sitemap (url, date, priority, freq)
        $sitemap->add(url('/'), date('c'), '1.0', 'daily');
        $sitemap->add(url('/auth/connexion'), date('c'), '0.5', 'monthly');
        $sitemap->add(url('/auth/inscription'), date('c'), '0.5', 'monthly');
        $sitemap->add(url('/account'), date('c'), '0.7', 'monthly');
        $sitemap->add(url('/evenements'), date('c'), '1.0', 'monthly');
        $sitemap->add(url('/evenements/rechercher'), date('c'), '0.8', 'monthly');
        $sitemap->add(url('/evenements/recherche_avancee'), date('c'), '0.8', 'monthly');
        $sitemap->add(url('/evenements/agenda'), date('c'), '0.9', 'monthly');
        $sitemap->add(url('/evenements/ajouter'), date('c'), '0.7', 'monthly');
        $sitemap->add(url('/contact'), date('c'), '0.6', 'monthly');
        $sitemap->add(url('/mentions-legales'), date('c'), '0.5', 'monthly');

    }

    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $sitemap->render('xml');
});