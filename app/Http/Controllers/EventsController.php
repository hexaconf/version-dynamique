<?php

namespace App\Http\Controllers;

use App\Event;
use App\Tag;
use App\Price;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use Validator;
use Spatie\Geocoder\GeocoderFacade;

class EventsController extends Controller
{
    public function index()
    {
        $events = Event::actives();
        return view('event.events', ['events' => $events]);
    }

    /**
     * @param $events
     * @return array
     */
    public function getCoordinates($events)
    {
        $coordinates = [];
        foreach ($events as $event) {
            $eventCoordinate = GeocoderFacade::getCoordinatesForQuery($event->address);
            $coordinates[] = array_merge($eventCoordinate, ["title" => $event->title, "url" => url('/evenements/' . $event->id)]);
        }
        return $coordinates;
    }

    public function show($id)
    {
        $event = Event::find($id);

        if ($event->valid != 1 && (Auth::check() && (Auth::user()->status != 1  || Auth::user()->id != $event->user->id))) {
            abort(404);
        }
        return view('event.event', ['event' => $event]);
    }

    public function store(Request $request)
    {
        $event = new Event();

        $errors = Validator::make($request->except('_token', 'tags', 'price_value', 'price_label', 'presentation_picture'), Event::$validator)->errors();

        if (!empty($errors->all()))
            return Redirect::to('evenements/ajouter')->withInput($request->except('tags', 'price_value', 'price_label', 'presentation_picture'))->withErrors($errors);

        if (!empty($request->input('tags'))) {
            $values = explode(',', $request->input('tags'));
            foreach ($values as $value) {
                if (Tag::where('title', $value)->count() == 0) {
                    $tag = Tag::create(['title' => $value, 'active' => false]);
                    $tag->save();
                }
            }
        }

        $event->fill($request->except('_token', 'tags', 'price_value', 'price_label', 'presentation_picture'));
        $event->user_id = Auth::user()->id;

        // saving to put an id to the event (useful next)
        $event->save();

        if ($request->hasFile('presentation_picture') && $request->file('presentation_picture')->isValid()) {
            $file = $request->file('presentation_picture');
            $movedFile = $file->move(public_path('img/pictures/'));
            $img = Image::make($movedFile->getRealPath());
            $img->fit(600, 400)->save(public_path('img/pictures/normal/' . $event->id . "." . $file->getClientOriginalExtension()));
            $img->fit(350, 200)->save(public_path('img/pictures/miniatures/' . $event->id . ".mini." . $file->getClientOriginalExtension()));
            $event->presentation_picture = $event->id . '.' . $file->getClientOriginalExtension();
        }

        if (!empty($request->input('price_value'))) {
            $event->deletePrices();
            for ($i = 0; $i < count($request->input('price_value')); $i++) {
                $price = Price::create(['value' => $request->input('price_value')[$i],
                    'label' => $request->input('price_label')[$i],
                    'event_id' => $event->id]);
                $price->save();
            }
        }

        if (isset($values)) {
            foreach ($values as $value) {
                $tag = Tag::where('title', $value)->first();
                $event->tags()->attach($tag->id);
            }
        }

        if (Auth::user()->status == 1) {
            $event->valid = 1;
        }
        $event->save();
        return Redirect::to('evenements/');
    }

    public function edit($id)
    {
        $event = Event::findOrFail($id);
        return view("event.form", ['event' => $event, 'isUpdate'=> true]);
    }

    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        if (Auth::user()->status != 1 && Auth::user()->id != $event->id) {
            abort(403);
        }
        $errors = Validator::make($request->except('_token', 'tags', 'price_value', 'price_label', 'presentation_picture'), Event::$validatorUpdate)->errors();

        if (!empty($errors->all()))
            return Redirect::to('evenements/'.$event->id.'/modifier')->withInput($request->except('tags', 'price_value', 'price_label', 'presentation_picture'))->withErrors($errors);

        if (!empty($request->input('tags'))) {
            $values = explode(',', $request->input('tags'));
            foreach ($values as $value) {
                if (Tag::where('title', $value)->count() == 0) {
                    $tag = Tag::create(['title' => $value, 'active' => false]);
                    $tag->save();
                }
            }
        }

        $event->fill($request->except('_token', 'tags', 'price_value', 'price_label', 'presentation_picture'));
        $event->user_id = Auth::user()->id;

        if ($request->hasFile('presentation_picture') && $request->file('presentation_picture')->isValid()) {
            $file = $request->file('presentation_picture');
            $movedFile = $file->move(public_path('img/pictures/'));
            $img = Image::make($movedFile->getRealPath());
            $img->fit(600, 400)->save(public_path('img/pictures/normal/' . $event->id . "." . $file->getClientOriginalExtension()));
            $img->fit(350, 200)->save(public_path('img/pictures/miniatures/' . $event->id . ".mini." . $file->getClientOriginalExtension()));
            $event->presentation_picture = $event->id . '.' . $file->getClientOriginalExtension();
        }

        if (!empty($request->input('price_value'))) {
            $event->deletePrices();
            foreach ($request->input('price_value') as $key => $priceInput) {
                $price = Price::create(['value' => $priceInput,
                    'label' => $request->input('price_label')[$key],
                    'event_id' => $event->id]);
                $price->save();
            }
        }

        if (isset($values)) {
            foreach ($values as $value) {
                $tag = Tag::where('title', $value)->first();
                if (! $event->tags->contains($tag->id)) {
                    $event->tags()->attach($tag->id);
                }

            }
        }
        $event->save();
        return Redirect::to('evenements/'.$event->id);
    }

    public function showAgenda()
    {
        $events = Event::actives();
        return view('agenda', ['events' => $events]);
    }

    public function create()
    {
        $event = new Event();
        return view("event.form", ['event' => $event, 'isUpdate'=> false]);
    }

    public function validatesEvent($id)
    {
        if (Auth::user()->status == 1) {
            $event = Event::find($id);
            $event->valid = !$event->valid;
            $event->save();
        }

        return Redirect::to('admin/events');
    }

    public function getTitles()
    {
        return Event::all()->pluck('title');
    }

    public function searchByTag($tag)
    {
        if ($tag == null) {
            abort(404);
        }
        $matchingEvents = Event::withTag($tag);
        return view('event.events', ['events' =>$matchingEvents]);
    }

    public function search(Request $request)
    {
        $matchingEvents = Event::getFromQuery($request->input('query'));
        return view('event.events', ['events' => $matchingEvents]);
    }

    public function advancedSearch(Request $request)
    {
        $matchingEvents =
            Event::getFromParameters($request->input('title'), $request->input('date'),
                $request->input('address'));
        return view('event.events', [
            'events' => $matchingEvents,
            'input' => $request->input()]);
    }

    public function getEventsSortByStatus()
    {
        return Event::all()->sortBy('valid')->sortByDesc('created_at');
    }

    public function eventsWithCoordinates()
    {
        $coordinates = $this->getCoordinates(Event::actives());
        return \Response::json($coordinates);
    }
}