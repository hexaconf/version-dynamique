<?php

namespace App\Http\Controllers;

use App\Opinion;
use Illuminate\Http\Request;

use App\Http\Requests;

class OpinionsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!\Auth::check()) {
            abort(404);
        }
        $this->validate($request, [
            'note' => 'required|integer|min:0|max:6',
            'event_id' => 'required|integer'
        ]);
        $opinion = new Opinion();
        $opinion->score = $request->input('note');
        $opinion->user_id = \Auth::user()->id;
        $opinion->event_id = $request->input('event_id');
        $opinion->save();
        return "OK";
    }
}
