<?php

namespace App\Http\Controllers;

use App\Alert;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


use App\Http\Requests;

class AlertsController extends Controller
{
    /**
     * AlertsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $alerts = Alert::allOfUser(\Auth::user());
        return view('alerts.index')->with(compact('alerts'));
    }

    public function getHtmlTableListingAlerts(){
        $alerts = Alert::allOfUser(\Auth::user());
        return view('alerts.table')->with(compact('alerts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $event_id
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'days_before' => 'required|integer',
            'event_id' => 'required|integer'
        ]);
        $alert = new Alert();
        $alert->event_id = $request->event_id;
        $alert->days_before = $request->days_before;
        $alert->user_id = \Auth::user()->id;
        $alert->save();
        return response()->json([
            'id'           => $alert->id,
            'event'        => $alert->event->title,
            'date'         => $alert->date,
            'days_before'  => $alert->days_before,
            'delete_route' => route('alertes.destroy', ['alertes'=>$alert->id])]);
    }

    public function destroy(Request $request, $id)
    {
        if ($request->user() == Auth::user()) {
            Alert::destroy($id);
        }
        return Redirect::to('alertes/');
    }
}