<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class PagesController extends Controller
{
    public function index()
    {
        $bestEvent = Event::getBest();
        $nextEvent = Event::getNext();
        return view("accueil", compact('bestEvent', 'nextEvent'));
    }

    public function legalMentions()
    {
        return view("mentions-legales");
    }

    public function contact()
    {
        return view("contact");
    }
}