<?php

namespace App\Http\Controllers;

use App\Alert;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmailTestController extends Controller
{
    public function index() {
        $alert = Alert::find(1);
        return view('emails.alert', compact('alert'));
    }
}
