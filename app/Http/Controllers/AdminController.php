<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->status != 1)
            return Redirect::to('/home');
        return view('admin.events', ['events' => Event::all()]);
    }

    public function events()
    {
        if (Auth::user()->status != 1)
            return Redirect::to('/home');
        return view('admin.events', ['events' => Event::all()]);
    }

    public function users()
    {
        if (Auth::user()->status != 1)
            return Redirect::to('/home');
        return view('admin.users', ['users' => User::all()]);
    }

}
