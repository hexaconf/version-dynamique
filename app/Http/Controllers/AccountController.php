<?php

namespace App\Http\Controllers;

use App\Alert;
use App\Opinion;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view("account.form", ['user' => Auth::user()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validation = $this->validator($request->all());
        $errors = $validation->errors()->all();

        $user = Auth::user();

        if (!$validation->fails()) {
            if ($request->get('new_password') == $request->get('new_password_confirm') || empty($request->get('new_password_confirm'))) {
                $data = $request->all();
                $user->update([
                    'firstname' => $data['firstname'],
                    'lastname' => $data['lastname'],
                    'login' => $data['login'],
                    'email' => $data['email'],
                    'password' => bcrypt($request->get('new_password_confirm'))]);
            } else {
                array_push($errors, "La nouveau password ne correspond pas");
            }
        }
        return Redirect::to('/account')->withInput($request->all)->withErrors($errors);
    }


    protected function validator(array $request)
    {
        return Validator::make($request, [
            'firstname' => 'required|max:55',
            'lastname' => 'required|max:55',
            'email' => 'required',
            'login' => 'required',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Quick fix, should work but could be cleaner
        Alert::where('user_id', $id)->forceDelete();
        Opinion::where('user_id', $id)->forceDelete();
        User::destroy($id);
        return Redirect::back();
    }

    /**
     * Create a json array containing all users
     *
     * @return JSon
     */
    public function getUsers() {
        return User::all();
    }
}
