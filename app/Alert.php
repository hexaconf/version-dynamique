<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Alert extends Model
{
    /**
     * @param User
     * @return Collection
     */
    public static function allOfUser($user)
    {
        return Alert::where('user_id', $user->id)->get();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function event() {
        return $this->belongsTo('App\Event');
    }

    public function getDateAttribute() {
        $date = new Carbon($this->event->begin_date);
        return $date->subDays($this->days_before)->format('d/m/y');
    }

    protected $fillable = ['days_before', 'user_id', 'event_id'];
}