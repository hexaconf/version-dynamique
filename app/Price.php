<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Price extends Model
{
    protected $fillable = ['value', 'label', 'event_id'];

    public function event()
    {
        $this->belongsTo('App\Event');
    }
}
