<?php

namespace App\Console;

use App\Alert;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function() {
            $alerts = Alert::all();
            foreach($alerts as $alert) {
                $eventDate = $alert->event->begin_date;
                $today = Carbon::today();
                /**
                 * Check if today is the day to send the alert
                 */
                if ($today->addDays($alert->days_before) == $eventDate)
                {
                    Mail::send('emails.alert', ['alert' => $alert], function ($m) use ($alert) {
                        $m->from(env('hexaconf@gmail.com'), 'Hexaconf');

                        $m->to($alert->user->email, $alert->user->fullname)->subject('Ne manquez pas l\'événement '.$alert->event->title.' !');
                    });
                }
            }
        })->dailyAt('10:00');
    }
}
